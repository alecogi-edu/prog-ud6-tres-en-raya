# PROG-UD6 - Juego del Tres En Raya - Versión 1 (Solución)

## Enunciado
Vamos a desarrollar el famoso juego del **tres en raya**. Se trata de un juego entre 2 jugadores; 0 y X, los cuales van introduciendo sus fichas en un tablero de 3x3. 
El jugador ganador será aquel que consiga colocar 3 fichas en línea ya sea en posición horizontal,  vertical, o en  diagonal.
La siguiente imagen muestra una partida en la que el jugador X ha ganado

![ejemplo-fin-ganado](/resources/ejemplo-fin-ganado.png)
   
Por otro lado, la partida también puede finalizar en empate si se completa todo el tablero y ningún jugador ha conseguido poner 3 de sus fichas en línea.

![ejemplo-fin-empate](/resources/ejemplo-fin-empate.png)
        
Se pide, desarrollar un programa, que nos permita jugar al “tres en raya”, utilizando la metodología de programación orientada a objetos POO.
 
A continuación, se definen el diagrama de clases completo:

![diagrama-uml](/resources/tres_en_raya_uml.png)

# Ampliación I (Solución)

Puedes consultar el resultado de la ampliación I en la [siguiente rama](https://gitlab.com/alecogi-edu/prog-ud6-tres-en-raya/-/tree/feature_amplicacion_1_2021).




