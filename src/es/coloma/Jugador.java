package es.coloma;

public class Jugador {

    private EstadoCasilla color;

    public Jugador(EstadoCasilla color) {
        assert color == EstadoCasilla.FICHA_O || color == EstadoCasilla.FICHA_X;
        this.color = color;
    }

    public void ponerFicha(Tablero tablero) {
        System.out.println("Tira el jugador con " + color);
        Coordenada coordenada = this.recogerCoordenadaValida(tablero);
        tablero.ponerFicha(coordenada, color);
    }

    private Coordenada recogerCoordenadaValida(Tablero tablero) {
        do {
            Coordenada coordenada = recogerCoordenada();
            if (tablero.isOcupada(coordenada)) {
                System.out.println("Coordenada ocupada en el tablero");
            } else {
                return coordenada;
            }
        } while (true);
    }
      
    public void cantaVictoria() {
        System.out.println("CAMPEONESSS! Los " + color + " son los mejores");
    }

    private Coordenada recogerCoordenada() {
        do {
            int fila = GestorIO.obtenerEntero("Introduce fila [1-" + Tablero.DIMENSION + "]: ");
            int columna = GestorIO.obtenerEntero("Introduce columna [1- " + Tablero.DIMENSION + "]: ");
            Coordenada coordenada = new Coordenada(fila, columna);
            if (!coordenada.esValida(Tablero.DIMENSION)) {
                System.out.println("Error!!! Valores fuera de rango\n");
            } else {
                return coordenada;
            }
        } while (true);
    }
}
