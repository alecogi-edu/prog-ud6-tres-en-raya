package es.coloma;

import java.util.Scanner;

public class GestorIO {

    private static Scanner scanner;

    static {
        scanner = new Scanner(System.in);
    }

    public static int obtenerEntero(String mensaje) {
        do {
            System.out.print(mensaje);
            if (!scanner.hasNextInt()) {
                System.out.println("Debe introducir un entero");
                scanner.next();
            } else {
                return scanner.nextInt();
            }
        } while (true);
    }

    public static boolean confirmar(String mensaje) {
        System.out.println(mensaje + " [S/N]");
        return scanner.next().toUpperCase().charAt(0) == 'S';
    }

}