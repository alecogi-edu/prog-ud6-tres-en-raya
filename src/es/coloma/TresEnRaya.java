package es.coloma;

public class TresEnRaya {

    private Tablero tablero;

    private Jugador[] jugadores;

    public static void main(String[] args){
        TresEnRaya tresEnRaya = new TresEnRaya();
        do {
            tresEnRaya.jugar();
        } while (preguntarNuevaPartida());
        System.out.println("Adéu");
    }

    public TresEnRaya(){
        jugadores = new Jugador[2];
        jugadores[0] = new Jugador(EstadoCasilla.FICHA_X);
        jugadores[1] = new Jugador(EstadoCasilla.FICHA_O);
        tablero = new Tablero();
    }

    public void jugar(){
        int turno = 0;
        tablero.mostrar();
        do {
            jugadores[turno].ponerFicha(tablero);
            tablero.mostrar();
            if (tablero.hayTresEnRaya()) {
                jugadores[turno].cantaVictoria();
                break;
            } else if (tablero.estaLleno()) {
                System.out.println("Empate");
                break;
            }
            turno = (turno + 1) % 2;
        } while (true);
        tablero.vaciar();
    }

    public static boolean preguntarNuevaPartida() {
        return GestorIO.confirmar("¿Quieres volver a jugar");
    }

}
