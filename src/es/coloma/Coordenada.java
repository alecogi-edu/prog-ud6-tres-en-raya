package es.coloma;

public class Coordenada {

    private int fila;

    private int columna;

    public Coordenada(int fila, int columna) {
        this.fila = fila;
        this.columna = columna;
    }

    public int getFila() { return fila; }

    public int getColumna() {
        return columna;
    }

    public boolean esValida(int dimension) {
        final int LIMITE_INFERIOR = 1;
        return fila >= LIMITE_INFERIOR && columna >= LIMITE_INFERIOR
                && fila <= dimension && columna <= dimension;
    }
}
