package es.coloma;

public class Tablero {

    private final EstadoCasilla[][] casillas;

    public static final int DIMENSION = 3;

    public Tablero() {
        casillas = new EstadoCasilla[DIMENSION][DIMENSION];
        vaciar();
    }

    public void mostrar() {
        drawSeparatorLine();
        for (int i = 0; i < DIMENSION; i++) {
            for (int j = 0; j < DIMENSION; j++) {
                System.out.printf("| %1s ", casillas[i][j]);
            }
            System.out.println("|");
            drawSeparatorLine();
        }
        System.out.println();
    }

    private void drawSeparatorLine() {
        System.out.print("+");
        for (int i = 0; i < DIMENSION ; i++) {
            System.out.print("----+");
        }
        System.out.println();
    }


    public boolean hayTresEnRaya() {
        return hayTresEnRaya(EstadoCasilla.FICHA_X)
                || hayTresEnRaya(EstadoCasilla.FICHA_O);
    }

    public boolean isOcupada(Coordenada coordenada) {
        return casillas[coordenada.getFila() - 1][coordenada.getColumna() - 1] != EstadoCasilla.VACIA;
    }

    public void ponerFicha(Coordenada coordenada, EstadoCasilla ficha) {
        casillas[coordenada.getFila() - 1][coordenada.getColumna() - 1] = ficha;
    }

    public void vaciar() {
        for (int i = 0; i < DIMENSION; i++) {
            for (int j = 0; j < DIMENSION; j++) {
                casillas[i][j] = EstadoCasilla.VACIA;
            }
        }
    }

    public boolean estaLleno() {
        for (int i = 0; i < DIMENSION; i++) {
            for (int j = 0; j < DIMENSION; j++) {
                if (casillas[i][j] == EstadoCasilla.VACIA) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean hayTresEnRaya(EstadoCasilla color) {
        int totalDiagonal = 0;
        int totalSubdiagonal = 0;
        for (int i = 0; i < DIMENSION; i++) {
            int totalHorizontal = 0;
            int totalVertical = 0;
            for (int j = 0; j < DIMENSION; j++) {
                if (color == casillas[i][j]) {
                    totalHorizontal++;
                    if (i == j) {
                        totalDiagonal++;
                    }
                    if ((i + j) == DIMENSION - 1) {
                        totalSubdiagonal++;
                    }
                }
                if (color == casillas[j][i]) {
                    totalVertical++;
                }
            }
            if (totalHorizontal == DIMENSION || totalVertical == DIMENSION
                    || totalDiagonal == DIMENSION || totalSubdiagonal == DIMENSION ){
                return true;
            }
        }
        return false;
    }

}
