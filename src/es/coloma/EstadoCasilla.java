package es.coloma;

public enum EstadoCasilla {
     FICHA_O (){
        @Override
        public String toString() {
            return "\uD83D\uDD35";
        }
    }, FICHA_X () {
        @Override
        public String toString() {
            return "\uD83D\uDD34";
        }
    }, VACIA () {
        @Override
        public String toString() {
            return "--";
        }
    }
}
